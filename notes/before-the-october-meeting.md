# Annotation Questions

## 018...

* Should "droit des affaires droit des PME" be annotated as a MWN?
* Since the above MWN is a modifier of "DESS", we attach it directly to the latter's NP?
  Or should te MWN be assigned its own NP?
  - The theory could be that modifiers and arguments must be enclosed in their own phrases,
    while the head is just a POS or MWEPOS
* "d'accord" -- typically annotated as a MWADV, but not in "de se metter d'accord" in FTB 
  (probably an example of inconsistency?)
* How the hell annotate "d'accord merci"?!
  - "d'accord" alone is a MWADV
  - in FTB, "merci" occurs twice in MWADV "sans merci", nothing else...
  - do we need to anotate it at all?
