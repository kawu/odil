<div class="panel panel-default" id="entities">
  <!--div class="panel-heading">Temporal entities</div-->
  <div class="panel-body">

    <h4>Marking events, signals, and temporal expressions</h4>
    <p>
      To annotate the selected node as an event, use the <b>Event</b> menu
      command.

      Similarly, the <b>Signal</b> menu command allows to annotate signals, and
      the <b>Timex</B> menu command -- temporal expressions.
    </p>
    <p>
      Note that, after you mark a particular node as a signal/event/timex, a
      set of dedicated attributes becomes available in the <b>Edit</b> side
      window, as shown in the example below.
    </p>
    <div class="row">
      <div class="col-sm-3"/>
      <div class="col-sm-6">
        <figure><center>
          <img src="public/img/guide/temporal/attributes.png" alt="Attributes" style="width:100%">
          <figcaption>Attributes available for annotation for the selected event</figcaption>
        </center></figure>
      </div>
      <div class="col-sm-3"/>
    </div>

    <h4>Anchoring</h4>
    <p>
      Most of the attributes have a prescribed, closed set of possible values.
      There are some which perform the function of relations between the
      selected node (of which the attributes are shown in the <b>Edit</b>
      window) and another node. For instance, each Timex can be anchored at
      another temporal entity, as shown below.
    </p>
    <p>
      TODO: need a good example!
    </p>

  </div>
</div>
