<div class="panel panel-default" id="relations">
  <!--div class="panel-heading">Temporal relations</div-->
  <div class="panel-body">
    <p>
      For the moment, there is only one way to create untyped relations:
      choose one node in each of the workspaces, top and bottom, and run the
      <b>connect</b> command line command. An untyped link between the two
      nodes (which can belong to two different trees) will be created. The
      direction of the link is always from the top tree to the bottom one. It
      can be deleted by selecting the circular node in the middle of the
      relation and pressing <b>d</b>.
    </p>

    <p>More to come...</p>
  </div>
</div>
