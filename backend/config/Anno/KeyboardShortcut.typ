-- Command invocation via a keyboard shortcut
{ keyCode : Integer
  -- ^ Key code corresponding to the `char`
, char : Text
  -- ^ The character of the shortcut
}
