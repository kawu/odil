TODO:
* Collect the files chosen by Anaïs (search for the mail entitled "ODIL : sélection données Accueil_UBS", 05/05/2017)
* Collect the remaining, not-so-nice files chosen by JY
* <hein>
* <e>

DONE:
* Collect the main files chosen by JY
* Example from JY-UBS/UTF-8/055_00000038.xml:
    <Turn speaker="spk1 spk2" startTime="12.857" endTime="15.984">
    <Sync time="12.857"/>
    <Who nb="1"/>
    hein vous pouvez tout à l'heure au revoir
    <Who nb="2"/>
    OK merci au revoir
    </Turn>
  <- How often does it happen that there are two speakers? 
